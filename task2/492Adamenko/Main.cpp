#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>

#include "Main.h"


class SampleApplication : public Application {
 public:
  MeshPtr _pseudosphere;
  float _N = 100.0;
  float _size = 0.7f;

  MeshPtr _marker; //Маркер для источника света

  ShaderProgramPtr _shader;
  ShaderProgramPtr _markerShader;

  //Координаты источника света
  float _lr = 3.;
  float _phi = 0.0;
  float _theta = glm::pi<float>() / 6;

  //скорость анимации
  float _animation_speed = 0.2;

  //Параметры источника света
  LightInfo _light;

  TexturePtr _firstTex;
  TexturePtr _secondTex;

  GLuint _sampler_first;
  GLuint _sampler_second;

  float _phase = 1.0;

  bool changeOverTimeEnabled = true;

  void makeScene() override {
    Application::makeScene();

    _pseudosphere = makePseudosphere(_size, _N);
    _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

    _marker = makeSphere(0.1f);
    //Инициализация шейдеров

    _shader = std::make_shared<ShaderProgram>("492AdamenkoData/common.vert", "492AdamenkoData/common.frag");
    _markerShader = std::make_shared<ShaderProgram>("492AdamenkoData/marker.vert", "492AdamenkoData/marker.frag");

    //Инициализация значений переменных освещения
    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    _light.ambient = glm::vec3(0.2, 0.2, 0.2);
    _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
    _light.specular = glm::vec3(1.0, 1.0, 1.0);
    _light.attenuation0 = 0.3;

    //Загрузка и создание текстур
    _firstTex = loadTexture("492AdamenkoData/earth_specular.png");
    _secondTex = loadTexture("492AdamenkoData/earth_global.jpg");

    //=========================================================
    //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
    glGenSamplers(1, &_sampler_first);
    glSamplerParameteri(_sampler_first, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler_first, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler_first, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler_first, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glGenSamplers(1, &_sampler_second);
    glSamplerParameteri(_sampler_second, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler_second, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler_second, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler_second, GL_TEXTURE_WRAP_T, GL_REPEAT);
  }

  void updateGUI() override {
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
      ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

      ImGui::Checkbox("Change texture over time", &changeOverTimeEnabled);
      ImGui::SliderFloat("Texture", &_phase, 0.0f, 1.0f);
      ImGui::SliderFloat("Animation speed", &_animation_speed, 0.0f, 0.5f);

      if (ImGui::CollapsingHeader("Light"))
      {
        ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
        ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
        ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

        ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
        ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
        ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

        ImGui::SliderFloat("attenuation", &_light.attenuation0, 0.01f, 10.0f);
      }
    }
    ImGui::End();

    if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
      if (_N <= 50) {
        _N += 1.0;
      } else {
        _N += 5.0;
      }
    }

    if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
      if (_N > 50) {
        _N -= 5.0;
      } else if (_N > 5.0) {
        _N -= 1.0;
      }
    }
    _pseudosphere = makePseudosphere(_size, _N);
    _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
  }

  void draw() override {

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Рисуем фигуру
    //Устанавливаем шейдер.
    _shader->use();

    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

    _shader->setVec3Uniform("light.pos", lightPosCamSpace);
    _shader->setVec3Uniform("light.La", _light.ambient);
    _shader->setVec3Uniform("light.Ld", _light.diffuse);
    _shader->setVec3Uniform("light.Ls", _light.specular);
    _shader->setFloatUniform("light.attenuation", _light.attenuation0);

    glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
    glBindSampler(0, _sampler_first);
    _firstTex->bind();
    _shader->setIntUniform("diffuseTexFirst", 0);

    glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
    glBindSampler(1, _sampler_second);
    _secondTex->bind();
    _shader->setIntUniform("diffuseTexSecond", 1);

    {
      _shader->setMat4Uniform("modelMatrix", _pseudosphere->modelMatrix());
      _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _pseudosphere->modelMatrix()))));

      float shift = (glfwGetTime() + 1) * _animation_speed;
      if (changeOverTimeEnabled) {
        _phase = (sin(shift) + 1) / 2;
      }
      _shader->setFloatUniform("shift", shift);
      _shader->setFloatUniform("phase", _phase);

      _pseudosphere->draw();
    }

    //Рисуем маркеры для источника света
    {
      _markerShader->use();

      _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
      _markerShader->setVec4Uniform("color", glm::vec4(_light.specular, 1.0f));
      _marker->draw();
    }

    //Отсоединяем сэмплер и шейдерную программу
    glBindSampler(0, 0);
    glUseProgram(0);
  }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}