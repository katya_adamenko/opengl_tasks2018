#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>

#include "Main.h"

class SampleApplication : public Application {
 public:
  MeshPtr _pseudosphere;
  float _N = 100.0;
  float _size = 2.5;

  ShaderProgramPtr _shader;

  void makeScene() override {
    Application::makeScene();
    _pseudosphere = makePseudosphere(_size, _N);
    _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

    //Создаем шейдерную программу
    _shader = std::make_shared<ShaderProgram>("492AdamenkoData/shaderNormal.vert", "492AdamenkoData/shader.frag");
  }

  void updateGUI() override {
    Application::updateGUI();

    if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
      if (_N <= 50) {
        _N += 1.0;
      } else {
        _N += 5.0;
      }
    }

    if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
      if (_N > 50) {
        _N -= 5.0;
      } else if (_N > 5.0) {
        _N -= 1.0;
      }
    }
    _pseudosphere = makePseudosphere(_size, _N);
    _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
  }

  void draw() override {
      Application::draw();

      int width, height;
      glfwGetFramebufferSize(_window, &width, &height);

      glViewport(0, 0, width, height);

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      //Устанавливаем шейдер.
      _shader->use();

      //Устанавливаем общие юниформ-переменные
      _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
      _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

      //Рисуем первый меш
      _shader->setMat4Uniform("modelMatrix", _pseudosphere->modelMatrix());
      _pseudosphere->draw();
  }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}