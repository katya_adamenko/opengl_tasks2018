#version 330

uniform mat4 mvpMatrix; //произведение матриц projectionMatrix * viewMatrix * modelMatrix

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord;

out vec2 texCoord; //текстурные координаты

void main()
{
	texCoord = vertexTexCoord;

	gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
}